# http://gl.developpez.com/tutoriel/outil/makefile/
# https://www.gnu.org/software/make/manual/html_node/File-Name-Functions.html

DEBUG ?= no
DEBUG ?= yes

##########################################
# program name
##########################################
PROGRAM_NAME = brightness-adjustor-$(TARGET)

##########################################
# input (source) / ouput (build) directories
##########################################
SRC_DIR = ./src/cross_platform/
SRC_DIR += ./lib/*/src/
OUT_ROOT = ./build/

##########################################
# configs
##########################################
FLAGS += -std=c++11
FLAGS += -ggdb `pkg-config --cflags opencv`

LD_FLAGS += `pkg-config --libs opencv`
LD_FLAGS += -lboost_system
LD_FLAGS += -lboost_filesystem

CC_FLAGS += -c

ifeq ($(DEBUG),yes)
	OUT_DIR += $(OUT_ROOT)debug/
	FLAGS += -g
else
	OUT_DIR += $(OUT_ROOT)release/
	FLAGS += -w
endif

ALIAS = $(OUT_ROOT)$(PROGRAM_NAME)

# SRC_LANG = c
SRC_LANG = c++

##########################################
# platform specific
##########################################
ifeq ($(OS),Windows_NT)
	FLAGS += -D WINDOWS
	SRC_FIND_PARAM += ./src/windows/
else
	UNAME_S += $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		FLAGS += -D LINUX
		SRC_FIND_PARAM += ./src/linux/
	endif
endif

##########################################
# settings
##########################################
ifdef TARGET
	SRC_FIND_PARAM += \( -not -path "*/_*" -or -path "*/_$(TARGET)/*" \)
else
	SRC_FIND_PARAM += -not -path "*/_*"
endif

# Put objects in the output directory
OBJ_DIR += $(OUT_DIR)object/
ifeq ($(SRC_LANG),c++)
	SRC_EXT = .cpp
else
	SRC_EXT = .c
endif
# Include all the source files here with the directory tree
SRC += $(shell find $(SRC_DIR) $(SRC_FIND_PARAM) -type f -name "*$(SRC_EXT)")
OBJ += $(patsubst %$(SRC_EXT), $(OBJ_DIR)%.o, $(notdir $(SRC)))
DIR += $(sort $(dir ${OUT_DIR} ${OBJ_DIR} ${OBJ}))
EXEC = $(OUT_DIR)$(PROGRAM_NAME)

##########################################
# executables
##########################################
MD := mkdir -p
RM := rm
ifeq ($(SRC_LANG),c++)
	CC := g++
else
	CC := gcc
endif

##########################################
# rules
##########################################
ifeq ($(DEBUG),yes)
all: debug $(EXEC)
else
all: $(EXEC)
endif

run: all
	@$(EXEC)

debug:
	@echo "DEBUG"
	@echo "OUT_DIR $(OUT_DIR)"
	@echo "OBJ_DIR $(OBJ_DIR)"
	@echo "DIR $(DIR)"
	@echo "SRC $(SRC)"
	@echo "OBJ $(OBJ)"
	@echo "EXEC $(EXEC)"
	@echo "ALIAS $(ALIAS)"
	@echo "FLAGS $(FLAGS)"
	@echo "CC_FLAGS $(CC_FLAGS)"
	@echo "LD_FLAGS $(LD_FLAGS)"

$(EXEC): rm_alias $(OBJ)
	@echo "Building: $@"
ifeq ($(DEBUG),yes)
	$(CC) -o $@ $(FLAGS) $(LD_FLAGS) $(OBJ)
else
	@$(CC) -o $@ $(FLAGS) $(LD_FLAGS) $(OBJ)
endif
	@ln -sr $@ $(ALIAS)

make_dir:
	@$(MD) ${DIR}

$(OBJ): make_dir $(SRC)
	@echo "Compiling: $(filter %$(notdir $*)$(SRC_EXT), $(SRC)) => $@"
ifeq ($(DEBUG),yes)
	$(CC) -o $@ $(FLAGS) $(CC_FLAGS) $(filter %$(notdir $*)$(SRC_EXT), $(SRC))
else
	@$(CC) -o $@ $(FLAGS) $(CC_FLAGS) $(filter %$(notdir $*)$(SRC_EXT), $(SRC))
endif

rm_alias:
	@$(RM) -rf $(ALIAS)

clean:
	@$(RM) -rf $(OBJ_DIR)

mrproper: clean rm_alias
	@$(RM) -rf $(OUT_DIR)

.PHONY: mrproper
