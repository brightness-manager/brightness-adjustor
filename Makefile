# http://gl.developpez.com/tutoriel/outil/makefile/
# https://www.gnu.org/software/make/manual/html_node/File-Name-Functions.html

DEBUG ?= no
# DEBUG = yes
export DEBUG

##########################################
# param
##########################################
install_directory =~/.bin


make = make --makefile=./Makefile_targeted_programm.make

ifeq ($(DEBUG),no)
	make += --jobs
endif

##########################################
# redirect all rules
##########################################
all:
	@$(make) TARGET=daemon
	@$(make) TARGET=gui

install: daemon
	@mkdir -p ${install_directory}
	@find $(abspath build) -type f -executable -exec ln -fs {} ${install_directory} \;
	@#export PATH=${PATH}:${install_directory}

daemon:
	@$(make) TARGET=daemon

gui:
	@$(make) TARGET=gui

rungui:
	@$(make) run TARGET=gui

rundaemon:
	@$(make) run TARGET=daemon

%:
	@$(make) $@ TARGET=daemon
	@$(make) $@ TARGET=gui
