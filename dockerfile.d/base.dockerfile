FROM debian:jessie

# install dependancies
RUN apt-get update && apt-get install -y --no-install-recommends \
	make \
	g++ \
	libopencv-dev \
	libboost-filesystem-dev \
	xbacklight

# install json library
RUN mkdir -p /tmp/brightness-adjustor/lib
# ADD ./nlohmann/json/archive/v2.0.1.tar.gz /tmp/brightness-adjustor/lib/
ADD https://github.com/nlohmann/json/archive/v2.0.1.tar.gz /tmp/brightness-adjustor/lib/
RUN ln -s /tmp/brightness-adjustor/lib/json-2.0.1 /tmp/brightness-adjustor/lib/json

# copy source code
RUN mkdir -p /tmp/brightness-adjustor
WORKDIR /tmp/brightness-adjustor
COPY . /tmp/brightness-adjustor
