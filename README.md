# brightness-adjustor

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [brightness-adjustor](#brightness-adjustor)
	- [Install](#install)
	- [Usage](#usage)

<!-- /TOC -->

## Install

Installation is not done yet


## Usage

```sh
make # compile the programmes
# or
make daemon &>/dev/null # compile & run the daemon
make gui # compile & run the gui
```
