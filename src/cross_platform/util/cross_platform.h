#ifndef CROSS_PLATFORM_H
	#define CROSS_PLATFORM_H
	
	#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
		#ifndef IS_LINUX
			#define IS_LINUX 0
		#endif
		#ifndef IS_WINDOWS
			#define IS_WINDOWS 1
		#endif
	#elif defined(__gnu_linux__) || defined(__linux__) || defined(__CYGWIN__)
		#ifndef IS_LINUX
			#define IS_LINUX 1
		#endif
		#ifndef IS_WINDOWS
			#define IS_WINDOWS 0
		#endif
	#else
		#ifndef IS_LINUX
			#define IS_LINUX 0
		#endif
		#ifndef IS_WINDOWS
			#define IS_WINDOWS 0
		#endif
	#endif
#endif
