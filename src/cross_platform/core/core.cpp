#include "core.hpp"

void histogramDraw(cv::Mat& frame)
{
	cv::Mat image = frame;
	int HistR[257] = { 0 };
	int HistG[257] = { 0 };
	int HistB[257] = { 0 };

	for (int i = 0; i < image.rows; i++)
	{
		for (int j = 0; j < image.cols; j++)
		{
			cv::Vec3b intensity = image.at<cv::Vec3b>(cv::Point(j, i));
			int Red = intensity.val[0];
			int Green = intensity.val[1];
			int Blue = intensity.val[2];
			HistR[Red] = HistR[Red] + 1;
			HistB[Blue] = HistB[Blue] + 1;
			HistG[Green] = HistG[Green] + 1;
		}
	}

	cv::Mat HistPlotR(500, 256, CV_8UC3, cv::Scalar(0, 0, 0));
	cv::Mat HistPlotG(500, 256, CV_8UC3, cv::Scalar(0, 0, 0));
	cv::Mat HistPlotB(500, 256, CV_8UC3, cv::Scalar(0, 0, 0));

	for (int i = 0; i < 256; i = i + 2)
	{
		line(HistPlotR, cv::Point(i, 500), cv::Point(i, 500 - HistR[i]), cv::Scalar(0, 0, 255), 1, 8, 0);
		line(HistPlotG, cv::Point(i, 500), cv::Point(i, 500 - HistG[i]), cv::Scalar(0, 255, 0), 1, 8, 0);
		line(HistPlotB, cv::Point(i, 500), cv::Point(i, 500 - HistB[i]), cv::Scalar(255, 0, 0), 1, 8, 0);
	}

	cv::namedWindow("Red Histogram");
	cv::namedWindow("Green Histogram");
	cv::namedWindow("Blue Histogram");

	imshow("Red Histogram", HistPlotR);
	imshow("Green Histogram", HistPlotG);
	imshow("Blue Histogram", HistPlotB);
}

double getBrightness(const cv::Mat& frame)
{
	cv::Mat temp, color[3], lum;
	temp = frame;

	split(temp, color);

	color[0] = color[0] * 0.299;
	color[1] = color[1] * 0.587;
	color[2] = color[2] * 0.114;

	lum = color[0] + color[1] + color[2];

	cv::Scalar summ = sum(lum);

	return summ[0] / ((pow(2, 8) - 1)*frame.rows * frame.cols) * 2; //-- percentage conversion factor
}

double median(boost::circular_buffer<double> cb)
{
	double median;
	size_t size = cb.size();
	
	sort(cb.begin(), cb.end());
	
	if (size % 2 == 0)
	{
		median = (cb[size / 2 - 1] + cb[size / 2]) / 2;
	}
	else
	{
		median = cb[size / 2];
	}
	
	return median;
}

bool updateBrightness(Config & config, cv::VideoCapture & cap, boost::circular_buffer<double>& inertia)
{
	cv::Mat frame;
	cap >> frame;
	
	double brightness = getBrightness(frame) * 100;
	inertia.push_back(brightness);
	// std::cout << (int)brightness << " ";
	
	brightness = median(inertia);
	// std::cout << (int)brightness << std::endl;
	
	set_brightness(config, brightness);
	
	return cv::waitKey(config.refresh_interval) < 0;
}
