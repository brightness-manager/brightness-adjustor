#ifndef CORE_H
	#define CORE_H
	
	#include <iostream>
	#include <numeric>
	
	#include <opencv2/opencv.hpp>
	#include <boost/circular_buffer.hpp>
	
	#include "../config/Config.hpp"
	#include "set_brightness.hpp"
	
	void histogramDraw(cv::Mat& frame);
	double getBrightness(const cv::Mat& frame);
	double median(boost::circular_buffer<double> cb);
	bool updateBrightness(Config & config, cv::VideoCapture & cap, boost::circular_buffer<double>& inertia);
#endif
