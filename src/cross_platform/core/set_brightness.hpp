#ifndef SET_BRIGHTNESS_H
	#define SET_BRIGHTNESS_H
	
	#include "../config/Config.hpp"
	
	void set_brightness(Config & config, int brightness);
#endif
