#ifndef CONFIG_H
	#define CONFIG_H
	
	#include <string>
	#include <fstream>
	
	#include <boost/filesystem.hpp>
	
	#include "../../../lib/json/src/json.hpp"
	using JSON = nlohmann::json;
	
	#include "../util/util.hpp"
	
	struct Config
	{
		double adjustor_factor = 1.5;
		int adjustor_padding = 0;
		int refresh_interval = 30;
		int change_duration = 0;
		int inertia_length = 30;
		
		static std::string path();
        Config();
		void init_with_default_values();
		void init_with_config_file();
		void save();
	};
#endif
