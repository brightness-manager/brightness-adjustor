#include "Config.hpp"

std::string Config::path()
{
	return get_home_path()
		+ boost::filesystem::path::preferred_separator
		+ ".config"
		+ boost::filesystem::path::preferred_separator
		+ "brightness-adjustor"
		+ boost::filesystem::path::preferred_separator
		+ "setting.json";
}

Config::Config()
{
    init_with_config_file();
}

void Config::init_with_default_values()
{
	adjustor_factor = 1.5;
	adjustor_padding = 0;
	refresh_interval = 30;
	change_duration = 0;
	inertia_length = 30;
}

void Config::init_with_config_file()
{
	std::string path = Config::path();
	std::ifstream config_file(path);
	if (config_file.fail())
	{
		init_with_default_values();
		config_file.close();
		return;
	}
	
	JSON json_object;
	config_file >> json_object;
	
	config_file.close();
	
	adjustor_factor = json_object["adjustor_factor"];
	adjustor_padding = json_object["adjustor_padding"];
	refresh_interval = json_object["refresh_interval"];
	change_duration = json_object["change_duration"];
	inertia_length = json_object["inertia_length"];
}

void Config::save()
{
	JSON empty_array_explicit = JSON({});
	JSON json_object;
	json_object["adjustor_factor"] = adjustor_factor;
	json_object["adjustor_padding"] = adjustor_padding;
	json_object["refresh_interval"] = refresh_interval;
	json_object["change_duration"] = change_duration;
	json_object["inertia_length"] = inertia_length;
	
	std::string path = Config::path();
	
	boost::filesystem::path dir = boost::filesystem::path(path).parent_path();
	boost::filesystem::create_directories(dir);
	
	std::ofstream config_file(path);
	config_file << json_object.dump(4);
	config_file.close();
}
