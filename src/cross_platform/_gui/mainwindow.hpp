#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QAbstractButton>

#include "../config/Config.hpp"
#include "restart_daemon.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    QSystemTrayIcon* trayIcon;
    QMenu* menu;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void create_system_tray_icon();
    void apply();
    void resetInput();
    void setInput(Config& config);
private slots:
    void on_dialogButton_clicked(QAbstractButton* button);
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
