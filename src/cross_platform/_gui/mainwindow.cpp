#include "mainwindow.hpp"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    restart_daemon();

    create_system_tray_icon();

    setWindowIcon(QIcon(":/icon.png"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setInput(Config& config)
{
    ui->adjustor_factor->setValue(config.adjustor_factor);
    ui->adjustor_padding->setValue(config.adjustor_padding);
    ui->refresh_interval->setValue(config.refresh_interval);
    ui->change_duration->setValue(config.change_duration);
    ui->inertia_length->setValue(config.inertia_length);
}


void MainWindow::create_system_tray_icon()
{
    menu = new QMenu(this);
    menu->addAction("Open", [this](){show();});
    menu->addSeparator();
    menu->addAction("Quit", [](){stop_daemon(); QCoreApplication::quit();});

    trayIcon = new QSystemTrayIcon(this);
    //trayIcon->setActiveAction("Open", [this](){show();});
    trayIcon->setContextMenu(menu);
    trayIcon->setIcon(QIcon(":/icon.png"));
    trayIcon->show();

    Config config;
    setInput(config);
}


void MainWindow::resetInput()
{
    Config config;
    config.init_with_default_values();
    setInput(config);
}


void MainWindow::apply()
{
    Config config;

    config.adjustor_factor = ui->adjustor_factor->value();
    config.adjustor_padding = ui->adjustor_padding->value();
    config.refresh_interval = ui->refresh_interval->value();
    config.change_duration = ui->change_duration->value();
    config.inertia_length = ui->inertia_length->value();

    config.save();

    restart_daemon();
}


void MainWindow::on_dialogButton_clicked(QAbstractButton* button)
{
    if (ui->dialogButton->buttonRole(button) == ui->dialogButton->ResetRole)
        resetInput();
    else if (ui->dialogButton->buttonRole(button) == ui->dialogButton->ApplyRole)
    {
        apply();
        close();
    }
    else
        close();
}
