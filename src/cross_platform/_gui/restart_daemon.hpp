#ifndef RESTART_DAEMON_H
	#define RESTART_DAEMON_H

	#include <cstdlib>

    void restart_daemon();
    void stop_daemon();
#endif
