#include "daemon.hpp"

int main(int argc, char const *argv[])
{
	#if !IS_WINDOWS && !IS_LINUX
		std::cerr << "platform not supported" << std::endl;
		exit(1);
	#endif
	
	cv::VideoCapture cap(0); // open the default camera
	if (!cap.isOpened())  // check if we succeeded
		return 1;
	
	Config config;
	
	boost::circular_buffer<double> inertia(config.inertia_length);
	
	while (updateBrightness(config, cap, inertia));

	return 0;
}
