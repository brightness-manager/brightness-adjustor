#ifndef DAEMON_H
	#define DAEMON_H
	
	#include <iostream>
	
	#include <boost/circular_buffer.hpp>
	
	#include "../util/cross_platform.h"
	#include "../config/Config.hpp"
	#include "../core/core.hpp"
#endif
