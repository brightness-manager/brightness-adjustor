#include "util.hpp"

std::string get_home_path()
{
	char* home_path = getenv("HOME");
	if (home_path == NULL)
	{
		home_path = getpwuid(getuid())->pw_dir;
	}
	return std::string(home_path);
}
