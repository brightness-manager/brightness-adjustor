#include "../../cross_platform/_gui/restart_daemon.hpp"

void restart_daemon()
{
    system("killall brightness-adjustor-daemon; ~/.bin/brightness-adjustor-daemon &>/dev/null");
}

void stop_daemon()
{
    system("killall brightness-adjustor-daemon");
}
