#include "set_brightness.hpp"

void set_brightness(Config & config, int brightness)
{
	int set_brightness = (brightness / config.adjustor_factor) - config.adjustor_padding;
	if (set_brightness < 1)
	{
		set_brightness = 1;
	}
	std::string cmd = "xbacklight -time " + std::to_string(config.change_duration) + " -set " + std::to_string(set_brightness);
	system(cmd.c_str());
}
