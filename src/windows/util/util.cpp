#include "util.hpp"

std::string get_home_path()
{
    std::string home_path = std::string(getenv("USERPROFILE"));
    if (!home_path)
	{
        home_path = std::string(getenv("HOMEPATH")) + std::string(getenv("HOMEDRIVE"));
	}
    return home_path;
}
