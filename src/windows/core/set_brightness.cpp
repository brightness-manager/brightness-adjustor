#include "set_brightness.hpp"

CGammaRamp gammaRamp;

void set_brightness(Config & config, int brightness)
{
	gammaRamp.SetBrightness(NULL, (brightness>128) ? 128:brightness);
}
