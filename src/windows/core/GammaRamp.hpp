#ifndef GAMMARAMP_H_
	#define GAMMARAMP_H_
	/*
	CGammaRamp class

	Encapsulates the Gamma Ramp API and changes the brightness of
	the entire screen.

	Written by Nir Sofer.
	http://www.nirsoft.net
	*/
	#include <windows.h>
	#include <opencv2/opencv.hpp>
	#include <HighLevelMonitorConfigurationAPI.h>
	#include <windows/core/GammaRamp.h>
	
	#include "../../config/Config.hpp"
	
	
	class CGammaRamp
	{
	protected:
		// #if IS_WINDOWS
		HMODULE hGDI32;
		HDC hScreenDC;
		typedef bool(WINAPI *Type_SetDeviceGammaRamp)(HDC hDC, LPVOID lpRamp);
		// #endif

		Type_SetDeviceGammaRamp pGetDeviceGammaRamp;
		Type_SetDeviceGammaRamp pSetDeviceGammaRamp;

	public:

		CGammaRamp();
		~CGammaRamp();
		bool LoadLibrary();
		void FreeLibrary();
		bool LoadLibraryIfNeeded();
		bool SetDeviceGammaRamp(HDC hDC, LPVOID lpRamp);
		bool GetDeviceGammaRamp(HDC hDC, LPVOID lpRamp);
		bool SetBrightness(HDC hDC, WORD wBrightness);
	};
#endif
