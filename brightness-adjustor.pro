#-------------------------------------------------
#
# Project created by QtCreator 2016-07-14T20:15:15
#
#-------------------------------------------------

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = brightness-adjustor
TEMPLATE = app


SOURCES += \
    src/cross_platform/_gui/gui.cpp \
    src/cross_platform/_gui/mainwindow.cpp \
    src/cross_platform/config/Config.cpp

HEADERS += \
    src/cross_platform/_gui/mainwindow.hpp \
    src/cross_platform/config/Config.hpp \
    src/cross_platform/util/cross_platform.h \
    src/cross_platform/util/util.hpp \
    src/cross_platform/_gui/restart_daemon.hpp

FORMS += \
    src/cross_platform/_gui/mainwindow.ui

LIBS += \
    -lboost_system \
    -lboost_filesystem

DISTFILES += \
    build/release/brightness-adjustor-daemon \
    build/brightness-adjustor-daemon \
    docker-compose.yml \
    dockerfile.d/base.dockerfile \
    dockerfile.d/daemon.dockerfile \
    dockerfile.d/gui.dockerfile \
    Dockerfile \
    LICENCE.md \
    README.md

RESOURCES += \
    src/icon.qrc


win32 {
    SOURCES += \
        src/windows/util/util.cpp \
        src/windows/_gui/restart_daemon.cpp

}


linux-g++|linux-g++-64|unix {
    SOURCES += \
        src/linux/util/util.cpp \
        src/linux/_gui/restart_daemon.cpp
}
